# .vimrc

This project contains my .vimrc file. I use the
[vundle](https://github.com/VundleVim/Vundle.vim) project for managing my
plugins.

# License

This is licensed under the [WTFPL](http://www.wtfpl.net/) license.

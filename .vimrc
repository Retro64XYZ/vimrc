"""" VUNDLE SETUP
set nocompatible              " be iMproved, required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Managed Plugins
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'flazz/vim-colorschemes'
Plugin 'Valloric/YouCompleteMe'
Plugin 'Valloric/MatchTagAlways'
Plugin 'scrooloose/syntastic'
Plugin 'mhinz/vim-signify'
Plugin 'Raimondi/delimitMate'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'plasticboy/vim-markdown'
Plugin 'chrisbra/csv.vim'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'nvie/vim-flake8'
Plugin 'townk/vim-autoclose'
Plugin 'ledger/vim-ledger'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set shell=/bin/bash
syntax enable
colo badwolf
set hlsearch
set encoding=utf-8  " The encoding displayed.
set fileencoding=utf-8  " The encoding written to file.
" Folding Setup
set nocompatible
let php_folding=1
let javaScript_fold=1
let tex_fold_enabled=1
set foldmethod=syntax
" /Folding Setup
" Omnicomplete
set omnifunc=syntaxcomplete#Complete
" /Omnicomplete
set mouse=a
"set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set number
set pastetoggle=<F2>
set backspace=indent,eol,start
" Enable a color column
set colorcolumn=80
" More tabs
set tabpagemax=50
" :WP Command runs this
func! WordProcessorMode() 
  setlocal printoptions=number:y
  setlocal formatoptions+=ta
  setlocal tw=79
  setlocal noexpandtab 
  setlocal spell spelllang=en_us 
  setlocal spellfile=$HOME/.vim/spell/en.utf-8.add
  setlocal noautoindent
  setlocal nosmartindent
  setlocal complete+=s
  setlocal formatprg=par
  setlocal tabstop=5
  nnoremap j gj
  nnoremap k gk
  set ignorecase
endfu 
com! WP call WordProcessorMode()
" Crunch multi line text into single line
function! Crunch() abort
	normal! G
	normal! o
	g/^./ .,/^$/-1 join
endfunction
com! CRUNCH call Crunch()
" vim airline setup
set noshowmode
set laststatus=2
let g:airline#extensions#branch#enabled=1
let g:airline#extensions#whitespace#enabled=0
let g:airline_theme='badwolf'
let g:airline_powerline_fonts=1
" Airline initial setup
function! AirlineInit()
	let g:airline_section_a = airline#section#create(['mode',' ', 'branch'])
	let g:airline_section_b = airline#section#create([''])
	let g:airline_section_c = airline#section#create(['filetype'])
	let g:airline_section_x = airline#section#create([' '])
	let g:airline_section_y = airline#section#create([' ','hunks','%f'])
endfunction
autocmd VimEnter * call AirlineInit()
" Add different coloring to my vim diff
highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red
" Insert the date and time at the cursor
inoremap <expr> <C-f> strftime('%Y-%m-%dT%H:%M:%S-07:00')
" Find Tags using MatchTagAlways
let g:mta_filetypes = {
    \ 'html' : 1,
    \ 'xhtml' : 1,
    \ 'xml' : 1,
    \ 'py' : 1,
    \ 'php' : 1,
\}
" YouCompleteMe
" :set ft? to get filetype
let g:ycm_key_list_select_completion   = ['<C-j>', '<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-k>', '<C-p>', '<Up>']
let g:ycm_filetype_blacklist = { 'plaintex' : 1, 'csv' : 1 }
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" Add virtualenv support to YCM
"python with virtualenv support
py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF

" Press Space to turn off highlighting and clear any message already
" displayed.
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

" Syntastic
" Set the default standards for phpcs using the below command
" $ phpcs --config-set default_standard Squiz
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Python Development
autocmd Filetype python setlocal ts=4 sts=4 sw=4 tw=79 expandtab autoindent
let python_highlight_all=1

" Flag white space
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
highlight BadWhitespace ctermbg=red guibg=red

" Ledger Setup
let g:ledger_maxwidth = 79
